<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->config->item('website_title1');?>&nbsp;<?php echo $this->config->item('website_title2');?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" type="image/png" href="<?php echo $this->config->item('favicon_path');?>"/>
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>plugins/font-awesome/fonts/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>plugins/ionicons/css/ionicons.min.css"/>
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>additional/lunba.css"/>
  <link rel="stylesheet" href="<?php echo $this->config->item('resource_url');?>plugins/datepicker/datepicker3.css"/>
  <script src="<?php echo $this->config->item('resource_url');?>plugins/jQuery/jQuery-2.2.0.min.js"></script>
  <script src="<?php echo $this->config->item('resource_url');?>additional/lunba.js"></script>
</head>
<body class="hold-transition <?php echo $this->config->item('default_skin');?> sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?php echo $this->config->item('website_abbrv');?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo $this->config->item('website_title1');?></b>&nbsp;<?php echo $this->config->item('website_title2');?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $this->config->item('resource_url');?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $this->config->item('resource_url');?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo $this->config->item('resource_url');?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MENU</li>
          <li class="treeview">
            <a href="<?php echo base_url();?>Peserta_skkni">
              <i class="fa fa-dashboard"></i> <span>Data Peserta</span>
            </a>
          </li>
          <li class="treeview">
            <a href="<?php echo base_url();?>Data">
              <i class="fa fa-dashboard"></i> <span>Laporan</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <?php $this->load->view($view_file_path);?>

      <footer class="main-footer">
        <strong>Copyright &copy; <?php echo $this->config->item('website_copyright_year');?> </strong><?php echo $this->config->item('website_copyright');?>.
        <!-- Below are the template maker link, thank you ! -->
        <!--<strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
        reserved.-->
      </footer>
    </div>
    <!-- ./wrapper -->

    <script src="<?php echo $this->config->item('resource_url');?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>dist/js/app.min.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>dist/js/demo.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="<?php echo $this->config->item('resource_url');?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>

        $(function () {

            $('.table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("[data-mask]").inputmask();

            // $(".timepicker").timepicker({
            //   showInputs: false
            // });

            $('.datepicker').datepicker({
              autoclose: true,
              format: 'yyyy-mm-dd'
            });


        });

    </script>

</body>
</html>
