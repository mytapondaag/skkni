<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <?php
    if (! empty($this->session->flashdata('success'))) {
      echo $this->session->flashdata('success');
    }

    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Title</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mdl-input">Tambah</button>
        </div>
      </div>
      <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal Lahir</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
            <?php if (! empty($result)) :?>
                <?php foreach($result as $row) : ?>
                  <tr>
                      <td><?php echo $row['id'] ?></td>
                      <td><?php echo $row['tanggal_lahir'] ?></td>
                      <td><?php echo $row['jumlah'] ?></td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals -->
  <div class="modal fade modal-default" id="mdl-input">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Input</h4>
        </div>
        <?php echo form_open('Data/input');?>
        <div class="modal-body">
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nama" name="in_nama" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-nik" name="in_nik" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-hp" name="in_hp" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">E-Mail</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-mail" name="in_mail" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-skema" name="in_skema" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-rekomendasi" name="in_rekomendasi" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-terbit" name="in_tgl_terbit" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="in-tgl-lahir" name="in_tgl_lahir" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Organisasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="in-organisasi" name="in_organisasi" placeholder="#">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
<!-- // Modals -->
