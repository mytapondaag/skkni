<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <?php
    if (! empty($this->session->flashdata('success'))) {
      echo $this->session->flashdata('success');
    }

    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Title</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mdl-input">Tambah</button>
        </div>
      </div>
      <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th></th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>No. HP</th>
                    <th>Email</th>
                    <th>Skema</th>
                    <th>Rekomendasi</th>
                    <th>Tanggal Terbit</th>
                    <th>Tanggal Lahir</th>
                    <th>Organisasi</th>
                </tr>
            </thead>
            <tbody>
            <?php if (! empty($peserta)) :
                $no=1; foreach($peserta AS $row) : ?>
                  <tr>
                      <td align="center"><?php echo $no++;?></td>
                      <td><button type="button"
                        class="btn btn-block btn-warning btn-xs btn-edit"
                        data-edid="<?php echo htmlspecialchars($row['id']);?>"
                        data-ednama="<?php echo htmlspecialchars($row['nama']);?>"
                        data-ednik="<?php echo htmlspecialchars($row['NIK']);?>"
                        data-edhp="<?php echo htmlspecialchars($row['HP'])?>"
                        data-edemail="<?php echo htmlspecialchars($row['email']);?>"
                        data-edskema="<?php echo htmlspecialchars($row['skema']);?>"
                        data-edrekomendasi="<?php echo htmlspecialchars($row['rekomendasi']);?>"
                        data-edterbit="<?php echo htmlspecialchars($row['tanggal_terbit']);?>"
                        data-edlahir="<?php echo htmlspecialchars($row['tanggal_lahir']);?>"
                        data-edorganisasi="<?php echo htmlspecialchars($row['organisasi']);?>"
                        data-toggle="modal"
                        data-target="#mdl-edit">Ubah</button>

                      <button type="button"
                        class="btn btn-block btn-danger btn-xs btn-hapus"
                        data-hid="<?php echo htmlspecialchars($row['id']);?>"
                        data-hnama="<?php echo htmlspecialchars($row['nama']);?>"
                        data-toggle="modal"
                        data-target="#mdl-hapus">Hapus</button>
                      </td>
                      <td><?php echo $row['nama'] ?></td>
                      <td><?php echo $row['NIK'] ?></td>
                      <td><?php echo $row['HP'] ?></td>
                      <td><?php echo $row['email'] ?></td>
                      <td><?php echo $row['skema'] ?></td>
                      <td><?php echo $row['rekomendasi'] ?></td>
                      <td><?php echo $row['tanggal_terbit'] ?></td>
                      <td><?php echo $row['tanggal_lahir'] ?></td>
                      <td><?php echo $row['organisasi'] ?></td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade modal-default" id="mdl-edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Ubah</h4>
      </div>
      <?php echo form_open('Peserta_skkni/ubah');?>
      <div class="modal-body">
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Nama</label>
          <div class="col-sm-10">
            <input type="hidden" id="ub-id" name="ub_id"/>
            <input type="text" class="form-control" id="ub-nama" name="ub_nama" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-nik" name="ub_nik" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">HP</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-hp" name="ub_hp" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">E-Mail</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-mail" name="ub_mail" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-skema" name="ub_skema" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-rekomendasi" name="ub_rekomendasi" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
          <div class="col-sm-10">
            <input type="text" class="form-control datepicker" id="ub-tgl-terbit" name="ub_tgl_terbit" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
          <div class="col-sm-10">
            <input type="text" class="form-control datepicker" id="ub-tgl-lahir" name="ub_tgl_lahir" placeholder="#">
          </div>
        </div>
        <div class="form-group row">
          <label for="#" class="col-sm-2 control-label">Organisasi</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="ub-organisasi" name="ub_organisasi" placeholder="#">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>

<div class="modal fade modal-default" id="mdl-hapus">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Hapus Peserta</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-sm-12">
            Hapus peserta <label id="hdnama"></label> ?
          </div>
        </div>
      </div>
      <?php echo form_open('Peserta_skkni/hapus');?>
      <div class="modal-footer">
        <input type="hidden" id="hdid" name="hid"/>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
      </div>
      <?php echo form_close();?>
    </div>
  </div>
</div>


<script>

    $('.btn-edit').click(function(){
        var id      = $(this).data('edid');
        var nama    = $(this).data('ednama');
        var nik     = $(this).data('ednik');
        var hp      = $(this).data('edhp');
        var email   = $(this).data('edemail');
        var skema   = $(this).data('edskema');
        var rekomendasi = $(this).data('edrekomendasi');
        var tgl_terbit  = $(this).data('edterbit');
        var tgl_lahir   = $(this).data('edlahir');
        var organisasi  = $(this).data('edorganisasi');

        $('#ub-id').val(id);
        $('#ub-nama').val(nama);
        $('#ub-nik').val(nik);
        $('#ub-hp').val(hp);
        $('#ub-mail').val(email);
        $('#ub-skema').val(skema);
        $('#ub-rekomendasi').val(rekomendasi);
        $('#ub-tgl-terbit').val(tgl_terbit);
        $('#ub-tgl-lahir').val(tgl_lahir);
        $('#ub-organisasi').val(organisasi);
    });

    $('.btn-hapus').click(function(){
        var id   = $(this).data('hid');
        var nama = $(this).data('hnama');

        $('#hdid').val(id);
        $('#hdnama').text(nama);
    });

</script>
