<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Data extends CI_Controller {
        function __construct()
        {
            parent::__construct();
            $this->load->model('input_peserta');
        }
        public function index()
        {
            $data['result'] = $this->input_peserta->get();
            $data['view_file_path'] = 'input.php';
            $this->load->view('defaults/layout', $data);
        }

        public function input()
        {
            $this->form_validation->set_rules($this->config->item('input_data'));

            if ($this->form_validation->run() !== TRUE) {
                $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
                // $this->session->set_flashdata('error', '<div id="simpel">aslkdfjlasdjfklsadjf</div>');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $this->input_peserta->simpan($_POST);
            redirect($_SERVER['HTTP_REFERER']);
        }

    }
