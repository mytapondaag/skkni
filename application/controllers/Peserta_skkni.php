<?php
defined('BASEPATH') or exit('Direct access script is not allowed');

/*
 *Daftar Peserta dan datanya
 *
 *@author  : Gratia C. Pondag
 *@date    : 19.09.2018
 *versi   : 1.0
*/
class Peserta_skkni extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('peserta');
    }

/*
 *index, Menampilkan data peserta
 *
 *@date    : 19.09.2018
 *versi   : 1.0
*/
    public function index()
    {
      $data['peserta'] = $this->peserta->ambil_data();
      $data['view_file_path'] = 'data_peserta.php';
      $this->load->view('defaults/layout', $data);
    }

/*
 *Tambah data peserta
 *
 *@date    : 19.09.2018
 *versi   : 1.0
*/
    public function tambah()
    {
      $this->form_validation->set_rules($this->config->item('input_data'));

      if ($this->form_validation->run() !== TRUE) {
          $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
          redirect($_SERVER['HTTP_REFERER']);
      }

      $this->peserta->simpan_data($_POST);
      redirect($_SERVER['HTTP_REFERER']);
    }

/*
 *ubah data peserta
 *
 *@param    : string  $data   berisi array, post from view
 *@date    : 19.09.2018
 *versi   : 1.0
*/
    public function ubah()
    {

    if (! empty($this->input->post())) {
        $this->peserta->edit_data();
        $this->session->set_flashdata('success', alert('mengedit', 'Berhasil Mengubah Data'));

    } else {

        $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
        redirect($_SERVER['HTTP_REFERER']);
    }

    redirect($_SERVER['HTTP_REFERER']);
    }

    public function hapus()
    {
      $this->peserta->hapus_data();
      $this->session->set_flashdata('error', alert('menghapus', 'Berhasil Menghapus Data'));
      redirect($_SERVER['HTTP_REFERER']);
    }
}

?>
