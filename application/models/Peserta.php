<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');


    class Peserta extends CI_Model {

        public function ambil_data()
        {
          $sql    = 'Select * from data';
          $query    = $this->db->query($sql)->result_array();
        //  echo "<pre>" ;print_r($query); exit();
          return $query;
        }

        public function simpan_data()
        {
            $data = array(
                'nama'            => $_POST['in_nama'],
                'NIK'             => $_POST['in_nik'],
                'HP'              => $_POST['in_hp'],
                'email'           => $_POST['in_mail'],
                'skema'           => $_POST['in_skema'],
                'rekomendasi'     => $_POST['in_rekomendasi'],
                'tanggal_terbit'  => $_POST['in_tgl_terbit'],
                'tanggal_lahir'   => $_POST['in_tgl_lahir'],
                'organisasi'      => $_POST['in_organisasi']
            );

            return $this->db->insert('data', $data);
        }

        public function edit_data()
        {
          $data = array(
              'nama'              => $_POST['ub_nama'],
              'NIK'               => $_POST['ub_nik'],
              'HP'                => $_POST['ub_hp'],
              'email'             => $_POST['ub_mail'],
              'skema'             => $_POST['ub_skema'],
              'rekomendasi'       => $_POST['ub_rekomendasi'],
              'tanggal_terbit'    => $_POST['ub_tgl_terbit'],
              'tanggal_lahir'     => $_POST['ub_tgl_lahir'],
              'organisasi'        => $_POST['ub_organisasi']
          );

          $this->db->where('id', $_POST['ub_id']);
          return $this->db->update('data', $data);
        }

        public function hapus_data()
        {
          $sql = "DELETE FROM data WHERE id = ".$_POST['hid'].";";

          return $this->db->query($sql);
        }
    }
?>
