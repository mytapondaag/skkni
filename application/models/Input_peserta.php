<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Input_peserta extends CI_Model {

        public function simpan()
        {
            $data = array(
                'nama'            => $_POST['in_nama'],
                'NIK'             => $_POST['in_nik'],
                'HP'              => $_POST['in_hp'],
                'email'           => $_POST['in_mail'],
                'skema'           => $_POST['in_skema'],
                'rekomendasi'     => $_POST['in_rekomendasi'],
                'tanggal_terbit'  => $_POST['in_tgl_terbit'],
                'tanggal_lahir'   => $_POST['in_tgl_lahir'],
                'organisasi'      => $_POST['in_organisasi']
            );

            return $this->db->insert('data', $data);
        }

        public function get()
        {
            $sql = "CALL cetak_data_dua();";
            
            return $this->db->query($sql)->result_array();
        }

    }
