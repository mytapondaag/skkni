#skkni

#Hirarki Folder

Framework yang digunakan adalah CodeIgniter
Folder & file yang di akses:

  application
  |
  |____ config
  |     |___ autoload.php
  |     |___ database.php
  |     |___ routes.php
  |____ controllers
  |     |___ Dasar.php
  |     |___ Data.php
  |     |___ Peserta_skkni.php   
  |____ helpers
  |     |___ universal_helper.php
  |____ models
  |     |___ Input_peserta.php
  |     |___ Peserta.php
  |____ views
  |     |___ default
  |     |___ blank_fix.php
  |     |___ blank.php
  |     |___ data_peserta.php
  |     |___ input.php
  resources
  |     |___ additional
  |     |___ bootstrap
  |     |___ dist
  |     |___ plugins
  system
  .htaccess
